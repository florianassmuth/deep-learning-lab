#!/usr/bin/env zsh

#SBATCH --job-name=test

#SBATCH --output=test.out

#SBATCH --time=00:05:00

ml switch intel foss
ml load Python
python -m pip install --upgrade pip
python -m pip install --user virtenv
python -m virtenv venv
source ./venv/bin/activate
python -m pip install numpy
./main.py